package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.naming.spi.DirStateFactory.Result;

import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int round = 1;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors 
        
        Random random = new Random();
        /*
        int randomIndex = random.nextInt(3);
        String computer_choise = rpsChoices.get(randomIndex);
        System.out.println(computer_choise);
        */
        int HumanScore  = 0;
        int ComputerScore = 0;
        while (true) {
            System.out.println("Let's play round " + Integer.toString(round));
           String HumanChoice = readInput("Your choice (Rock/Paper/Scissors)?");
           int randomIndex = random.nextInt(3);
           String ComputerChoice = rpsChoices.get(randomIndex);
           String result = "";
            if (HumanChoice.equals("rock") || HumanChoice.equals("paper") || HumanChoice.equals("scissors")) {
                round ++;
                if (ComputerChoice.equals(HumanChoice)) {
                    result = "It's a tie!";
                } 
                else if (ComputerChoice.equals("rock") && HumanChoice.equals("scissors") || ComputerChoice.equals("paper") && HumanChoice.equals("rock") || ComputerChoice.equals("scissors") && HumanChoice.equals("paper")) {
                    result = "Computer wins!";
                    ComputerScore ++;
                }
                else {
                    result = "Human wins!";
                    HumanScore ++;
                }
                System.out.println("Human chose " + HumanChoice + ", computer chose " + ComputerChoice + ". " + result);
                System.out.println("Score: human " + Integer.toString(HumanScore) + ", computer " + Integer.toString(ComputerScore));
                String countinue = readInput("Do you wish to continue playing? (y/n)?"); 
                if (countinue.equals("n")) {
                    System.out.println("Bye bye :)");
                    break;
                }
            }
            else {
                System.out.println("I do not understand cardboard. Could you try again?");
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
